package otus.ru.dummy.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/hello")
public class HelloWorldController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity getHelloMessage() {
        return ResponseEntity.ok()
                             .body("Hello colleagues!!!");
    }
}
