package otus.ru.dummy.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(HelloWorldController.class)
class HelloWorldControllerTest {

    @Autowired
    private MockMvc mvc;

    private final static String URL = "/hello/";
    private final static String ANSWER = "Hello colleagues!!!";

    @Test
    void getHelloMessage() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get(URL))
           .andExpect(status().isOk())
           .andExpect(content().string(equalTo(ANSWER)));

    }
}